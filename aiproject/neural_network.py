import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt


# prepoznavanje rukom pisane cifre na osnovu slike

# ucitavanje podataka
data = keras.datasets.mnist

# podelu na train i test podatke
(train_images, train_labels), (test_images, test_labels) = data.load_data()

class_names = ["0", "1", "2", "3", "6", "5", "6", "7", "8", "9"]

# smanjivanje velicina podataka
train_images = train_images/255.0
test_images = test_images/255.0


# definisanje modela neuronske mreze

# model1
# definisanje slojeva
model1 = keras.Sequential([keras.layers.Flatten(input_shape=(28, 28)), # input # 28 redova po 28 piksela
                          keras.layers.Dense(233, activation="relu"), # slojevi su potpuno povezani # skriveni sloj # aktivaciona funkcija - Rectified Linear Unit max(0, z)
                          keras.layers.Dense(10, activation="softmax")]) # ukupna vrednost svih izlaza je 1

# algoritam za optimizaciju
model1.compile(optimizer="adam", # izmena tezina
              loss="sparse_categorical_crossentropy", # loss funkcija
              metrics=["accuracy"])

# obucavanje u 4 epohe
model1.fit(train_images, train_labels, epochs=4)

test_loss1, test_accuracy1 = model1.evaluate(test_images, test_labels)
print("Zavrseno obucavanje modela1\n\n\n")





# model2
model2 = keras.Sequential([keras.layers.Flatten(input_shape=(28, 28)),
                          keras.layers.Dense(233, activation="relu"),
                          keras.layers.Dense(250, activation="tanh"), # Hyperbolic tangent
                          keras.layers.Dense(10, activation="softmax")])

model2.compile(optimizer="adam",
              loss="sparse_categorical_crossentropy",
              metrics=["accuracy"])

model2.fit(train_images, train_labels, epochs=5)

test_loss2, test_accuracy2 = model2.evaluate(test_images, test_labels)
print("Zavrseno obucavanje modela2\n\n\n")




# model3
model3 = keras.Sequential([keras.layers.Flatten(input_shape=(28, 28)),
                          keras.layers.Dense(233, activation="relu"),
                          keras.layers.Dense(250, activation="tanh"),
                          keras.layers.Dense(210, activation="relu"),
                          keras.layers.Dense(10, activation="softmax")])

model3.compile(optimizer="sgd", # Stochastic gradient descent
              loss="sparse_categorical_crossentropy",
              metrics=["accuracy"])

model3.fit(train_images, train_labels, epochs=5)

test_loss3, test_accuracy3 = model3.evaluate(test_images, test_labels)
print("Zavrseno obucavanje modela3\n\n\n")




print("Tacnosti nakon obucavanja:")
print("Model1: ", test_accuracy1)
print("Model2: ", test_accuracy2)
print("Model3: ", test_accuracy3)

model_name = ""
model = model1

# provera koji model ima najvecu tacnost
if((test_accuracy1 > test_accuracy2) and (test_accuracy1 > test_accuracy3)):
    print("Model1 IMA NAJVECU TACNOST")
    model = model1
    model_name = "model1.h5"
elif((test_accuracy1 < test_accuracy2 and test_accuracy3 < test_accuracy2)):
    print("Model2 IMA NAJVECU TACNOST")
    model = model2
    model_name = "model2.h5"
elif((test_accuracy3 > test_accuracy1 and test_accuracy3 > test_accuracy2)):
    print("Model3 IMA NAJVECU TACNOST")
    model = model3
    model_name = "model3.h5"



# cuvanje modela koji je imao najvecu tacnost
model.save(model_name)

# ucitavanje sacuvanog modela
# model = keras.models.load_model(model_name)

# predvidjanje u koju klasu spada slika
prediction = model.predict(test_images)


# prikaz par predikcija sa slika  
for i in range(10):
    plt.imshow(test_images[i+1255], cmap=plt.cm.binary)
    plt.title("Predvidjena klasa: " + class_names[np.argmax(prediction[i+1255])])
    plt.show()

